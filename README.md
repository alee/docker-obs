To Build and Run:
================
1. Build minbase image, execute:
$ sudo make minbase

2. Build images for OBS server and OBS api to run on different containers
   - OBS server container image
   - OBS api container image
   - OBS worker container image

To build that images and launch in multiple containers with compose, execute:
$ sudo make
$ sudo docker-compose up

3. Add more workers
$ sudo docker-compose scale obs-worker=2

Troubleshooting:

1. docker compose fails to start
   See https://github.com/docker/compose/issues/1113
   Some cleanup might be needed if you are doing tests.
   $ docker ps -a
   $ docker rm CONTAINER_ID (listed by previous command)

As a primer on Docker:
======================

* build it
`docker build --tag my-image-name:tag .`

* run it
`docker run --name my-container-name -d my-image-name:tag`

* check the logs
`docker logs my-container-name`

* list running containers
`docker ps`

* list all containers
`docker ps -a`

more info on docker commands:
* https://github.com/wsargent/docker-cheat-sheet
